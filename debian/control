Source: libsearch-xapian-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Salvatore Bonaccorso <carnil@debian.org>,
           Olly Betts <olly@survex.com>,
           Nick Morrott <knowledgejunkie@gmail.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libdevel-leak-perl <!nocheck>,
               libtest-pod-perl <!nocheck>,
               libxapian-dev,
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libsearch-xapian-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libsearch-xapian-perl.git
Homepage: https://xapian.org/
Rules-Requires-Root: no

Package: libsearch-xapian-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Suggests: xapian-doc
Description: Perl bindings for the Xapian search library
 Search::Xapian provides Perl bindings for the Xapian Open Source Search
 Engine library.
 .
 The Xapian search engine library is a highly adaptable toolkit which allows
 developers to easily add advanced indexing and search facilities to their own
 applications. It implements the probabilistic model of information retrieval,
 and provides facilities for performing ranked free-text searches, relevance
 feedback, phrase searching, boolean searching, stemming, and simultaneous
 update and searching. It is highly scalable and is capable of working with
 collections containing hundreds of millions of documents.
